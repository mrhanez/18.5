﻿#include <iostream>
#include <string>
#include <algorithm>

class Player {
public:
    std::string name;
    int score;
};

bool comparePlayers(const Player& player1, const Player& player2) {
    return player1.score > player2.score;
}

int main() {
    int numPlayers;
    std::cout << "Сколько игроков вы хотите добавить? ";
    std::cin >> numPlayers;

    Player* players = new Player[numPlayers];

    for (int i = 0; i < numPlayers; i++) {
        std::cout << "Введите имя игрока " << i + 1 << ": ";
        std::cin >> players[i].name;
        std::cout << "Введите количество очков для игрока " << i + 1 << ": ";
        std::cin >> players[i].score;
    }

    std::sort(players, players + numPlayers, comparePlayers);

    std::cout << "Имена и очки игроков в отсортированном виде:" << std::endl;
    for (int i = 0; i < numPlayers; i++) {
        std::cout << players[i].name << ": " << players[i].score << std::endl;
    }

    delete[] players;

    return 0;
}
